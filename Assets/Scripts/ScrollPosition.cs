﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollPosition : MonoBehaviour
{
    public Slider slider;
    public ScrollRect scrollRect;
    // Use this for initialization

    public void ChangeScrollPos()
    {
        Rect rtContent = scrollRect.content.rect;
        Rect rtViewport = scrollRect.viewport.rect;
        if (rtContent.height > rtViewport.height)
            scrollRect.verticalNormalizedPosition = slider.value;
        else
            slider.value = 1;
    }
}
