﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class ChatManager : MonoBehaviour {

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public InputField msgField;
    public bool isZoomIn = false;
    public bool isRoom = false;
    public bool isGame = false;

    public GameObject zoomOutPanel;
    public GameObject zoomInPanel;
    public ScrollRect zoomOutScrollView;
    public ScrollRect zoomInScrollView;
    public Text zoomOutContent;
    public Text zoomInContent;
    public GameObject zoomOutWorldButton;
    public GameObject zoomInWorldButton;
    public GameObject zoomOutRoomButton;
    public GameObject zoomInRoomButton;
    public Sprite[] WorldButtonImages;
    public Sprite[] RoomButtonImages;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        if (!SmartFoxConnection.IsInitialized)
        {
            SceneManager.LoadScene("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        zoomOutPanel.SetActive(!isZoomIn);
        zoomInPanel.SetActive(isZoomIn);

        zoomOutWorldButton.SetActive(isGame);
        zoomInWorldButton.SetActive(isGame);
        zoomOutRoomButton.SetActive(isGame);
        zoomInRoomButton.SetActive(isGame);

        zoomOutWorldButton.GetComponent<Image>().sprite = WorldButtonImages[isRoom ? 0 : 1];
        zoomInWorldButton.GetComponent<Image>().sprite = WorldButtonImages[isRoom ? 0 : 1];
        zoomOutRoomButton.GetComponent<Image>().sprite = RoomButtonImages[isRoom ? 1 : 0];
        zoomInRoomButton.GetComponent<Image>().sprite = RoomButtonImages[isRoom ? 1 : 0];

        GlobalInfo.roomText = "";
        zoomOutContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
        zoomInContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void onArrow()
    {
        isZoomIn = !isZoomIn;
        zoomOutPanel.SetActive(!isZoomIn);
        zoomInPanel.SetActive(isZoomIn);
    }

    public void onWorldRoom(int cmd)
    {
        isRoom = (cmd == 0) ? false : true;

        zoomOutWorldButton.GetComponent<Image>().sprite = WorldButtonImages[isRoom ? 0 : 1];
        zoomInWorldButton.GetComponent<Image>().sprite = WorldButtonImages[isRoom ? 0 : 1];
        zoomOutRoomButton.GetComponent<Image>().sprite = RoomButtonImages[isRoom ? 1 : 0];
        zoomInRoomButton.GetComponent<Image>().sprite = RoomButtonImages[isRoom ? 1 : 0];

        zoomOutContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
        zoomInContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
        zoomOutScrollView.verticalNormalizedPosition = 0;
        zoomInScrollView.verticalNormalizedPosition = 0;
    }

    public void onSend()
    {
        if (msgField.text != "")
        {
            ISFSObject obj = new SFSObject();
            obj.PutUtfString("senderName", GlobalInfo.name);
            // Send public message to Room
            if(isRoom)
                sfsManager.sfs.Send(new Sfs2X.Requests.PublicMessageRequest(msgField.text, obj, sfsManager.sfs.LastJoinedRoom));
            else
            {
                obj.PutUtfString("message", msgField.text);
                sfsManager.sfs.Send(new ExtensionRequest("worldMessage", obj));
            }

            // Reset message field
            msgField.text = "";
            msgField.ActivateInputField();
            //msgField.Select();
        }
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void printMessage(bool bRoom)
    {
        if(isRoom == bRoom)
        {
            zoomOutContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
            zoomInContent.text = isRoom ? GlobalInfo.roomText : GlobalInfo.worldText;
            zoomOutScrollView.verticalNormalizedPosition = 0;
            zoomInScrollView.verticalNormalizedPosition = 0;
        }
    }
}
