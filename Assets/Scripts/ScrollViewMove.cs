﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewMove : MonoBehaviour {

    public ScrollRect scrollRect;
    public Slider slider;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void OnValChange()
    {
        slider.value = scrollRect.verticalNormalizedPosition;
    }
}
