﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDrop : MonoBehaviour, IDropHandler {

    public GameManager gameManager;
    public int backNo;

    public Transform item
    {
        get
        {
            if (transform.childCount > 0)
                return transform.GetChild(0);
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        int cardNo = CardDrag.itemBeingDragged.GetComponent<CardDrag>().cardNo;
        int index = -1;
        for (int i = 0; i < 5; i++)
        {
            if (gameManager.arrangePos[i] == backNo)
                index = i;
        }
        if (index == -1)
        {
            gameManager.setCard(cardNo, backNo);
            //print(cardNo + "=" + backNo);
        }
        else
        {
            int tmp = gameManager.arrangePos[cardNo];
            gameManager.setCard(cardNo, gameManager.arrangePos[index]);
            gameManager.setCard(index, tmp);
        }

    }
}
