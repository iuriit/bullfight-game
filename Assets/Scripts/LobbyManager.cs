﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class LobbyManager : MonoBehaviour {

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject chatPanel;
    public GameObject settingsPanel;

    public Text nameText;
    public Text chipText;
    public Text cashText;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            SceneManager.LoadScene("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        setUserInfo();
        //enableUI(false);
        //sfsManager.sfs.Send(new JoinRoomRequest("The Lobby"));
    }

    // Update is called once per frame
    void Update () {
		
	}

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void onExit()
    {
        sfsManager.sfs.Disconnect();
    }

    public void onPlayGame()
    {
        SceneManager.LoadScene("RoomList");
    }

    public void onSettings()
    {
        settingsPanel.SetActive(true);
    }

    public void onSettingsClose()
    {
        settingsPanel.SetActive(false);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void enableUI(bool enable)
    {
        canvas.GetComponent<CanvasGroup>().interactable = enable;
    }

    public void setUserInfo()
    {
        nameText.text = GlobalInfo.name;
        chipText.text = GlobalInfo.userInfo.GetInt("chip").ToString();
        cashText.text = (GlobalInfo.userInfo.GetInt("chip") * 0.01f).ToString();
    }
}
