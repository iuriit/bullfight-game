﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class LoginManager : MonoBehaviour
{

    //----------------------------------------------------------
    // Editor public properties
    //----------------------------------------------------------

    [Tooltip("IP address or domain name of the SmartFoxServer 2X instance")]
    public string Host = "127.0.0.1";

    [Tooltip("TCP port listened by the SmartFoxServer 2X instance; used for regular socket connection in all builds except WebGL")]
    public int TcpPort = 9933;

    [Tooltip("WebSocket port listened by the SmartFoxServer 2X instance; used for in WebGL build only")]
    public int WSPort = 8080;

    [Tooltip("Name of the SmartFoxServer 2X Zone to join")]
    public string Zone = "BullFight";

    //----------------------------------------------------------
    // UI elements
    //----------------------------------------------------------

    public GameObject canvas;
    public GameObject loginPanel;
    public GameObject registerPanel;
    public InputField loginPanelUsername;
    public InputField loginPanelPassword;
    public GameObject rememberUsernameCheck;
    public GameObject rememberPasswordCheck;
    public Text loginPanelErrorText;
    public InputField registerPanelUsername;
    public InputField registerPanelPassword;
    public InputField registerPanelConfirmPassword;
    public InputField registerPanelReferences;
    public InputField registerPanelMobilePhone;
    public Text registerPanelErrorText;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();
        Application.runInBackground = true;
        if (PlayerPrefs.GetInt("rememberUsername") == 1)
            loginPanelUsername.text = PlayerPrefs.GetString("username");
        if (PlayerPrefs.GetInt("rememberPassword") == 1)
            loginPanelPassword.text = PlayerPrefs.GetString("password");
        rememberUsernameCheck.SetActive(PlayerPrefs.GetInt("rememberUsername") == 1);
        rememberPasswordCheck.SetActive(PlayerPrefs.GetInt("rememberPassword") == 1);
    }

    //----------------------------------------------------------
    // Public interface methods for UI
    //----------------------------------------------------------

    public void onRememberUsernameClick()
    {
        rememberUsernameCheck.SetActive(!rememberUsernameCheck.active);
        PlayerPrefs.SetInt("rememberUsername", rememberUsernameCheck.active ? 1 : 0);
    }

    public void onRememberPasswordClick()
    {
        rememberPasswordCheck.SetActive(!rememberPasswordCheck.active);
        PlayerPrefs.SetInt("rememberPassword", rememberPasswordCheck.active ? 1 : 0);
    }

    public void onValueChange()
    {
        loginPanelErrorText.text = "";
        registerPanelErrorText.text = "";
    }

    public void onLogin()
    {
        loginPanelErrorText.text = "";
        if (loginPanelUsername.text == "")
        {
            loginPanelUsername.ActivateInputField();
            loginPanelErrorText.text = "Username is invalid.";
        }
        else if (loginPanelPassword.text == "")
        {
            loginPanelPassword.ActivateInputField();
            loginPanelErrorText.text = "Password is invalid.";
        }
        else
        {
            GlobalInfo.isLogin = true;
            setConnection();
        }
    }

    public void onToRegister()
    {
        registerPanelErrorText.text = "";
        loginPanel.SetActive(false);
        registerPanel.SetActive(true);
    }

    public void onRegister()
    {
        registerPanelErrorText.text = "";
        if (registerPanelUsername.text == "")
        {
            registerPanelUsername.ActivateInputField();
            registerPanelErrorText.text = "Username is invalid.";
        }
        else if (registerPanelPassword.text == "")
        {
            registerPanelPassword.ActivateInputField();
            registerPanelErrorText.text = "Password is invalid.";
        }
        else if (registerPanelPassword.text != registerPanelConfirmPassword.text)
        {
            registerPanelPassword.ActivateInputField();
            registerPanelErrorText.text = "Password does not match.";
        }
        else if (registerPanelReferences.text == "")
        {
            registerPanelReferences.ActivateInputField();
            registerPanelErrorText.text = "References is invalid.";
        }
        else if (registerPanelMobilePhone.text == "")
        {
            registerPanelMobilePhone.ActivateInputField();
            registerPanelErrorText.text = "Mobile phone is invalid.";
        }
        else
        {
            GlobalInfo.isLogin = false;
            setConnection();
        }
    }

    public void onToLogin()
    {
        loginPanelErrorText.text = "";
        loginPanel.SetActive(true);
        registerPanel.SetActive(false);
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void enableUI(bool enable)
    {
        canvas.GetComponent<CanvasGroup>().interactable = enable;
    }

    public void login(SFSObject obj)
    {
        if(obj.GetBool("success"))
        {
            GlobalInfo.isNewLogin = true;
            GlobalInfo.username = loginPanelUsername.text;
            GlobalInfo.password = loginPanelPassword.text;
            GlobalInfo.name = PlayerPrefs.GetString("name", GlobalInfo.username);
            sfsManager.sfs.Send(new LogoutRequest());
        }
        else
        {
            sfsManager.sfs.Disconnect();
            if (obj.GetUtfString("reason") == "username")
            {
                loginPanelErrorText.text = "Username does not exist.";
                loginPanelUsername.ActivateInputField();
            }
            else if (obj.GetUtfString("reason") == "password")
            {
                loginPanelErrorText.text = "Forgot password?";
                loginPanelPassword.ActivateInputField();
            }
        }
    }

    public void register(SFSObject obj)
    {
        if (obj.GetBool("success"))
        {
            enableUI(true);
            loginPanelUsername.text = registerPanelUsername.text;
            loginPanelPassword.text = registerPanelPassword.text;
            onToLogin();
        }
        else
        {
            if (obj.GetUtfString("reason") == "username")
            {
                registerPanelErrorText.text = "Username already exists.";
                registerPanelUsername.ActivateInputField();
            }
        }
    }

    public void setConnection()
    {
        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = Host;
#if !UNITY_WEBGL
        cfg.Port = TcpPort;
#else
		cfg.Port = WSPort;
#endif
        cfg.Zone = Zone;

        // Initialize SFS2X client and add listeners
#if !UNITY_WEBGL
        sfsManager.sfs = new SmartFox();
        //sfs = new SmartFox();
#else
		sfsManager.sfs = new SmartFox(UseWebSocket.WS_BIN);
		//sfs = new SmartFox(UseWebSocket.WS_BIN);
#endif

        // Set ThreadSafeMode explicitly, or Windows Store builds will get a wrong default value (false)
        sfsManager.sfs.ThreadSafeMode = true;

        sfsManager.reset();
        enableUI(false);

        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION, sfsManager.OnConnection);
        sfsManager.sfs.AddEventListener(SFSEvent.CONNECTION_LOST, sfsManager.OnConnectionLost);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN, sfsManager.OnLogin);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGIN_ERROR, sfsManager.OnLoginError);
        sfsManager.sfs.AddEventListener(SFSEvent.LOGOUT, sfsManager.OnLogout);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN, sfsManager.OnRoomJoin);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, sfsManager.OnRoomJoinError);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_ADD, sfsManager.OnRoomAdded);
        sfsManager.sfs.AddEventListener(SFSEvent.ROOM_REMOVE, sfsManager.OnRoomRemoved);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, sfsManager.OnUserEnterRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, sfsManager.OnUserExitRoom);
        sfsManager.sfs.AddEventListener(SFSEvent.USER_COUNT_CHANGE, sfsManager.OnUserCountChange);
        sfsManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, sfsManager.OnExtensionResponse);
        sfsManager.sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, sfsManager.OnPublicMessage);

        // Connect to SFS2X
        sfsManager.sfs.Connect(cfg);
    }

}
