﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class SFSManager : MonoBehaviour
{
    public SmartFox sfs;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.LoadScene("Login");
    }

    // Update is called once per frame
    void Update()
    {
        if (sfs != null)
            sfs.ProcessEvents();
    }

    private void OnApplicationQuit()
    {
        sfs.Disconnect();
    }

    //----------------------------------------------------------
    // Public helper methods
    //----------------------------------------------------------

    public void reset()
    {
        // Remove SFS2X listeners
        // This should be called when switching scenes, so events from the server do not trigger code in this scene
        sfs.RemoveAllEventListeners();
        if(SceneManager.GetActiveScene().name == "Login")
        {
            LoginManager loginManager = (LoginManager)GameObject.FindObjectOfType(typeof(LoginManager));
            loginManager.enableUI(true);
        }
    }

    //----------------------------------------------------------
    // SmartFoxServer event listeners
    //----------------------------------------------------------

    public void OnConnection(BaseEvent evt)
    {
        if ((bool)evt.Params["success"])
        {
            Debug.Log("SFS2X API version: " + sfs.Version);
            Debug.Log("Connection mode is: " + sfs.ConnectionMode);

            // Save reference to SmartFox instance; it will be used in the other scenes
            SmartFoxConnection.Connection = sfs;

            // Login
            sfs.Send(new LoginRequest("", ""));
        }
        else
        {
            print("ConnectionFail");
            // Remove SFS2X listeners and re-enable interface
            reset();

            GameObject errorText = GameObject.Find("ErrorText");
            // Show error message
            if (errorText)
                errorText.GetComponent<Text>().text = "Connection Failed. Try again";
        }
    }

    public void OnConnectionLost(BaseEvent evt)
    {
        print("ConnectionLost");
        // Remove SFS2X listeners and re-enable interface
        reset();

        string reason = (string)evt.Params["reason"];

        if (reason != ClientDisconnectionReason.MANUAL)
        {
            GameObject errorText = GameObject.Find("ErrorText");
            // Show error message
            if (errorText)
                errorText.GetComponent<Text>().text = "Connection was lost; reason is: " + reason;
        }
        if(SceneManager.GetActiveScene().name != "Login")
            SceneManager.LoadScene("Login");
    }

    public void OnLogin(BaseEvent evt)
    {
        if(GlobalInfo.isNewLogin)
        {
            GlobalInfo.isNewLogin = false;
            if (PlayerPrefs.GetInt("rememberUsername") == 1)
                PlayerPrefs.SetString("username", GlobalInfo.username);
            if (PlayerPrefs.GetInt("rememberPassword") == 1)
                PlayerPrefs.SetString("password", GlobalInfo.password);
            sfs.Send(new JoinRoomRequest("The Lobby"));
            return;
        }

        LoginManager loginManager = (LoginManager)GameObject.FindObjectOfType(typeof(LoginManager));
        ISFSObject obj = new SFSObject();

        if (GlobalInfo.isLogin)
        {
            obj.PutUtfString("username", loginManager.loginPanelUsername.text);
            obj.PutUtfString("password", loginManager.loginPanelPassword.text);
            sfs.Send(new ExtensionRequest("login", obj));
        }
        else
        {
            obj.PutUtfString("username", loginManager.registerPanelUsername.text);
            obj.PutUtfString("password", loginManager.registerPanelPassword.text);
            obj.PutUtfString("reference", loginManager.registerPanelReferences.text);
            obj.PutUtfString("mobilephone", loginManager.registerPanelMobilePhone.text);
            sfs.Send(new ExtensionRequest("register", obj));
        }
    }

    public void OnLoginError(BaseEvent evt)
    {
        print("LoginError");
        GlobalInfo.isNewLogin = false;
        // Remove SFS2X listeners and re-enable interface
        reset();
        // Disconnect
        sfs.Disconnect();

        LoginManager loginManager = (LoginManager)GameObject.FindObjectOfType(typeof(LoginManager));
        // Show error message
        string errorMsg = (string)evt.Params["errorMessage"];
        loginManager.loginPanelErrorText.GetComponent<Text>().text = (string)evt.Params["errorMessage"];
    }

    public void OnLogout(BaseEvent evt)
    {
        if(GlobalInfo.isNewLogin)
        {
            sfs.Send(new LoginRequest(GlobalInfo.username, GlobalInfo.password));
        }
    }

    public void OnRoomJoin(BaseEvent evt)
    {
        Room room = (Room)evt.Params["room"];

        ISFSObject obj = new SFSObject();
        obj.PutUtfString("username", GlobalInfo.username);
        sfs.Send(new ExtensionRequest("getUserInfo", obj, null));

        Scene scene = SceneManager.GetActiveScene();
        if (room.Name == "The Lobby")
        {
            if(scene.name == "Game")
                SceneManager.LoadScene("RoomList");
            else
                SceneManager.LoadScene("Lobby");
        }
        else if(room.IsGame)
        {
            SceneManager.LoadScene("Game");
        }
    }

    public void OnRoomJoinError(BaseEvent evt)
    {
        // Remove SFS2X listeners and re-enable interface
        reset();
        // Disconnect
        sfs.Disconnect();
    }

    public void OnUserEnterRoom(BaseEvent evt)
    {
        User user = (User)evt.Params["user"];
    }

    public void OnUserExitRoom(BaseEvent evt)
    {
        User user = (User)evt.Params["user"];
    }

    public void OnRoomAdded(BaseEvent evt)
    {
        Room room = (Room)evt.Params["room"];
    }

    public void OnRoomRemoved(BaseEvent evt)
    {
    }

    public void OnUserCountChange(BaseEvent evt)
    {
        //sfs.Send(new ExtensionRequest("getRoomList", new SFSObject(), null));
    }

    public void OnPublicMessage(BaseEvent evt)
    {
        User sender = (User)evt.Params["sender"];
        string message = (string)evt.Params["message"];
        SFSObject dataObject = (SFSObject)evt.Params["data"];

        GlobalInfo.roomText += dataObject.GetUtfString("senderName") + ": " + message + "\n";
        ChatManager chatManager = (ChatManager)GameObject.FindObjectOfType(typeof(ChatManager));
        if (chatManager)
            chatManager.printMessage(true);
    }

    public void OnExtensionResponse(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        SFSObject dataObject = (SFSObject)evt.Params["params"];
        LoginManager loginManager = (LoginManager)GameObject.FindObjectOfType(typeof(LoginManager));
        ChatManager chatManager = (ChatManager)GameObject.FindObjectOfType(typeof(ChatManager));
        GameManager gameManager = (GameManager)GameObject.FindObjectOfType(typeof(GameManager));
        RoomListManager roomListManager = GameObject.FindObjectOfType<RoomListManager>();
        LobbyManager lobbyManager = GameObject.FindObjectOfType<LobbyManager>();

        Scene curScene = SceneManager.GetActiveScene();

        print(cmd);

        switch (cmd)
        {
            case "login":
                loginManager.login(dataObject);
                break;
            case "register":
                loginManager.register(dataObject);
                break;
            case "worldMessage":
                GlobalInfo.worldText += dataObject.GetUtfString("senderName") + ": " + dataObject.GetUtfString("message") + "\n";
                if (chatManager)
                    chatManager.printMessage(false);
                break;
            case "getRoomList":
                if (curScene.name == "RoomList")
                    roomListManager.populateRoomList(dataObject);
                break;
            case "getUserInfo":
                GlobalInfo.userInfo = dataObject.GetSFSObject("userInfo");
                if (curScene.name == "Lobby")
                    lobbyManager.setUserInfo();
                else if (curScene.name == "RoomList")
                    roomListManager.setUserInfo();
                break;
            case "ready":
                gameManager.buyinPanel.SetActive(!dataObject.GetBool("alreadyJoin"));
                break;
            case "setTimer":
                gameManager.setTimer(dataObject);
                break;
            case "showPlayer":
                gameManager.showPlayer(dataObject);
                break;
            case "dealFirstCards":
                gameManager.dealFirstCards(dataObject);
                break;
            case "setBanker":
                gameManager.setBanker(dataObject);
                break;
            case "dealSecondCards":
                gameManager.dealSecondCards(dataObject);
                break;
            case "autoArrange":
                gameManager.autoArrange(dataObject);
                break;
            case "hideArrangeCards":
                gameManager.hideArrangeCards();
                break;
            case "showSpecialWords":
                gameManager.showSpecialWords(dataObject);
                break;
            case "showCards":
                gameManager.showCards(dataObject);
                break;
            case "showWords":
                gameManager.showWords(dataObject);
                break;
            case "showPayout":
                gameManager.showPayout(dataObject);
                break;
            case "showResult":
                gameManager.showResult(dataObject);
                break;
            case "hideCards":
                gameManager.hideCards();
                break;
            case "exitRoom":
                gameManager.exitRoom();
                break;
            case "leaveRoom":
                print("leaveRoom");
                GlobalInfo.isExit = false;
                break;
            case "userCountChange":
                ISFSObject obj = new SFSObject();
                obj.PutInt("type", GlobalInfo.searchType);
                sfs.Send(new ExtensionRequest("getRoomList", obj, null));
                break;
        }
    }

}
