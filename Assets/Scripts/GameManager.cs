﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class GameManager : MonoBehaviour {

    // UI elements

    public GameObject chatPanel;
    public GameObject morePanel;
    public GameObject winPanel;
    public GameObject losePanel;
    public GameObject specialWordsPanel;
    public GameObject exitPanel;
    public GameObject buyinPanel;
    public GameObject settingsPanel;

    public Sprite[] cardImage;
    public GameObject[] player;
    public GameObject personalInfo;
    public Text[] playerName;
    public Text[] playerCash;
    public Image[] playerBox;
    public GameObject[] banker;
    public Sprite[] bankerImage;
    public GameObject[] card;
    public GameObject bid;
    public Image[] bidTimeline;
    public GameObject firstCard;
    public GameObject dealerMessage;
    public Text dealerTipText;
    public GameObject tipButton;
    public GameObject swipeCard;
    public GameObject bottomImage;
    public GameObject openButton;
    public GameObject switchCardButton;
    public GameObject arrange;
    public GameObject waiting;
    public Sprite[] bullWords;
    public Sprite[] numberImage;
    public GameObject alarm;
    public Sprite[] alarmNumberImage;
    public GameObject exitButton;

    public Text buyinPanelCurrentChipsText;
    public Text buyinPanelAmountChipsText;
    public Text buyinPanelCashText;
    public GameObject buyinPanelAutoMinBuyinCheck;
    public InputField buyinPanelBuyinInput;
    public GameObject buyinPanelBuyinSliderGroup;
    public Slider buyinPanelBuyinSlider;

    public Text tableName;
    public Text dateTimeText;

    // Public properties

    public int[] arrangePos = new int[5];

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;
    private Dictionary<string, int> playerPos = new Dictionary<string, int>();
    private int playerNum = 1;
    private float bidTimer = 0;
    private float timer = 0;
    private bool isSwitch = false;
    private float scaleTimer = 0f;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;

        if (!SmartFoxConnection.IsInitialized)
        {
            SceneManager.LoadScene("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        resObj.PutUtfString("name", GlobalInfo.name);
        sfsManager.sfs.Send(new ExtensionRequest("ready", resObj, sfsManager.sfs.LastJoinedRoom));

        for (int i = 0; i < 5; i++)
            arrangePos[i] = -1;
        onClear();
        initInfo();
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        timer = (timer > 0) ? timer : 0;
        int timerN = Mathf.CeilToInt(timer);
        Transform ts;
        ts = alarm.transform.Find("Ten");
        ts.gameObject.GetComponent<Image>().sprite = alarmNumberImage[timerN / 10];
        ts.gameObject.GetComponent<Image>().SetNativeSize();
        ts = alarm.transform.Find("One");
        ts.gameObject.GetComponent<Image>().sprite = alarmNumberImage[timerN % 10];
        ts.gameObject.GetComponent<Image>().SetNativeSize();

        if (bid.active)
        {
            bidTimer += Time.deltaTime;
            for (int i = 0; i < 4; i++)
                bidTimeline[i].fillAmount = 1 - bidTimer / 4;
        }

        DateTime dateNow = System.DateTime.Now;
        dateTimeText.text = dateNow.ToString("MM/dd/yyyy HH:mm:ss");

        if(specialWordsPanel.active)
        {
            scaleTimer += Time.deltaTime;
            ts = specialWordsPanel.transform.Find("BottomPlate");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
            ts = specialWordsPanel.transform.Find("Cards");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
            ts = specialWordsPanel.transform.Find("Word");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1.5f, 1.5f, 1), scaleTimer * 2f);
        }
        if (winPanel.active)
        {
            scaleTimer += Time.deltaTime;
            ts = winPanel.transform.Find("BottomPlate");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
            ts = winPanel.transform.Find("Text");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
        }
        if (losePanel.active)
        {
            scaleTimer += Time.deltaTime;
            ts = losePanel.transform.Find("BottomPlate");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
            ts = losePanel.transform.Find("Text");
            ts.localScale = Vector3.Lerp(new Vector3(0, 0, 1), new Vector3(1, 1, 1), scaleTimer * 2f);
        }
    }

    // Public UI methods

    public void onExit()
    {
        GlobalInfo.isExit = true;

        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        resObj.PutUtfString("name", GlobalInfo.name);
        sfsManager.sfs.Send(new ExtensionRequest("exitRoom", resObj, sfsManager.sfs.LastJoinedRoom));

        //exitPanel.SetActive(true);
    }

    public void onExitPanelYes()
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        resObj.PutUtfString("name", GlobalInfo.name);
        sfsManager.sfs.Send(new ExtensionRequest("exitRoom", resObj, sfsManager.sfs.LastJoinedRoom));

        exitButton.SetActive(false);
        exitPanel.SetActive(false);
    }

    public void onExitPanelNo()
    {
        exitPanel.SetActive(false);
    }

    public void onMore()
    {
        morePanel.SetActive(!morePanel.active);
    }

    public void onBidButton(float value)
    {
        ISFSObject obj = new SFSObject();
        obj.PutFloat("value", value);
        sfsManager.sfs.Send(new ExtensionRequest("bid", obj, sfsManager.sfs.LastJoinedRoom));
        bid.SetActive(false);
    }

    public void onOpenButton()
    {
        GlobalInfo.swipeCardNum = 0;
        openButton.SetActive(false);
        swipeCard.SetActive(false);
        bottomImage.SetActive(false);
        arrange.SetActive(true);
        switchCardButton.SetActive(GlobalInfo.cash >= 50 && !isSwitch);
        onClear();
    }

    public void onConfirm()
    {
        for(int i = 0; i < 5; i ++)
        {
            if (arrangePos[i] == -1)
                return;
        }
        int arrangeCode = 1;
        for (int i = 0; i < 5; i++)
            arrangeCode = arrangeCode * 5 + arrangePos[i];
        ISFSObject obj = new SFSObject();
        obj.PutInt("param", 0);
        obj.PutInt("value", arrangeCode);
        sfsManager.sfs.Send(new ExtensionRequest("arrange", obj, sfsManager.sfs.LastJoinedRoom));
    }

    public void onAutoArrange()
    {
        ISFSObject obj = new SFSObject();
        obj.PutInt("param", 1);
        sfsManager.sfs.Send(new ExtensionRequest("arrange", obj, sfsManager.sfs.LastJoinedRoom));
    }

    public void onClear()
    {
        for (int i = 0; i < 5; i++)
        {
            setCard(i, -1);
        }
    }

    public void onBuyinPanelCloseClick()
    {
        exitRoom();
    }

    public void onBuyinPanelAutoMinBuyinClick()
    {
        buyinPanelAutoMinBuyinCheck.SetActive(!buyinPanelAutoMinBuyinCheck.active);
        PlayerPrefs.SetInt("autoMinBuyin", buyinPanelAutoMinBuyinCheck.active ? 1 : 0);
        buyinPanelBuyinSliderGroup.GetComponent<CanvasGroup>().interactable = !buyinPanelAutoMinBuyinCheck.active;
        if(buyinPanelAutoMinBuyinCheck.active)
            onBuyinPanelMinButtonClick();
    }

    public void onBuyinPanelMinButtonClick()
    {
        buyinPanelBuyinInput.text = GlobalInfo.curRoomItem.buyin.ToString();
        buyinPanelBuyinSlider.value = GlobalInfo.curRoomItem.buyin;
    }

    public void onBuyinPanelMaxButtonClick()
    {
        buyinPanelBuyinInput.text = GlobalInfo.userInfo.GetInt("chip").ToString();
        buyinPanelBuyinSlider.value = GlobalInfo.userInfo.GetInt("chip");
    }

    public void onBuyinPanelSliderValChange()
    {
        buyinPanelBuyinInput.text = buyinPanelBuyinSlider.value.ToString();
    }

    public void onBuyinPanelConfirmClick()
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        resObj.PutUtfString("name", GlobalInfo.name);
        resObj.PutInt("buyin", (int)(buyinPanelBuyinSlider.value));
        sfsManager.sfs.Send(new ExtensionRequest("join", resObj, sfsManager.sfs.LastJoinedRoom));
        buyinPanel.SetActive(false);
        personalInfo.SetActive(true);
    }

    public void onTipButton()
    {
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        sfsManager.sfs.Send(new ExtensionRequest("tip", resObj, sfsManager.sfs.LastJoinedRoom));

        tipButton.SetActive(false);
        dealerMessage.SetActive(true);
        dealerTipText.text = "Thanks " + GlobalInfo.name + " for 50 tips. Good luck!";
        StartCoroutine(showTipButton());
    }

    IEnumerator showTipButton()
    {
        yield return new WaitForSeconds(5f);
        tipButton.SetActive(true);
        dealerMessage.SetActive(false);
    }

    public void onSwitchCard()
    {
        isSwitch = true;
        switchCardButton.SetActive(false);
        ISFSObject resObj = new SFSObject();
        resObj.PutUtfString("username", GlobalInfo.username);
        sfsManager.sfs.Send(new ExtensionRequest("switchCard", resObj, sfsManager.sfs.LastJoinedRoom));
    }

    public void onSettings()
    {
        settingsPanel.SetActive(true);
    }

    public void onSettingsClose()
    {
        settingsPanel.SetActive(false);
    }

    // Public Helpers

    public void initInfo()
    {
        tableName.text = GlobalInfo.curRoomItem.tableName;
        buyinPanelCurrentChipsText.text = GlobalInfo.userInfo.GetInt("chip").ToString();
        buyinPanelAmountChipsText.text = GlobalInfo.curRoomItem.buyin.ToString() + "-" + buyinPanelCurrentChipsText.text;
        buyinPanelCashText.text = (GlobalInfo.userInfo.GetInt("chip") * 0.01f).ToString();
        buyinPanelBuyinSlider.minValue = GlobalInfo.curRoomItem.buyin;
        buyinPanelBuyinSlider.maxValue = GlobalInfo.userInfo.GetInt("chip");
        buyinPanelBuyinSlider.value = buyinPanelBuyinSlider.minValue;
        buyinPanelAutoMinBuyinCheck.SetActive(PlayerPrefs.GetInt("autoMinBuyin") == 1);
        onBuyinPanelMinButtonClick();
    }

    public void exitRoom()
    {
        sfsManager.sfs.Send(new JoinRoomRequest("The Lobby"));
    }

    public void setTimer(SFSObject obj)
    {
        timer = obj.GetFloat("value");
    }

    public void showPlayer(SFSObject obj)
    {
        string username = obj.GetUtfString("username");
        string name = obj.GetUtfString("name");
        int cash = obj.GetInt("cash");
        bool isNew = obj.GetBool("isNew");
        bool isBusted = obj.GetBool("isBusted");
        if (!playerPos.ContainsKey(username))
        {
            if(username == GlobalInfo.username)
                playerPos.Add(username, 0);
            else
                playerPos.Add(username, playerNum++);
        }
        int pos = playerPos[username];
        cash = (cash > 0) ? cash : 0;

        if (pos == 0)
            GlobalInfo.cash = cash;

        player[pos].SetActive(true);
        playerName[pos].text = name;
        playerCash[pos].text = cash.ToString();
        playerBox[pos].color = new Color(1f, 1f, 1f, (isNew || isBusted) ? 0.5f : 1f);
        GlobalInfo.isNew = (pos == 0 && isNew);
        waiting.SetActive(GlobalInfo.isNew);
    }

    public void dealFirstCards(SFSObject obj)
    {
        Transform ts;
        string username = obj.GetUtfString("username");
        int pos = playerPos[username];
        bool isNew = obj.GetBool("isNew");

        if(pos == 0)
        {
            if(!isNew)
            {
                bid.SetActive(true);
                bidTimer = 0;
                firstCard.SetActive(true);
                for (int i = 0; i < 3; i++)
                {
                    ts = firstCard.transform.Find("ShowCards/Card" + (i + 1));
                    ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card" + i)];
                }
            }
        }
        else
        {
            card[pos].SetActive(true);
            for (int j = 1; j <= 5; j++)
            {
                ts = card[pos].transform.Find("BackCards/Card" + j);
                ts.gameObject.SetActive(j <= 3);
            }
            ts = card[pos].transform.Find("BackCards");
            ts.gameObject.SetActive(true);
            ts = card[pos].transform.Find("ShowCards");
            ts.gameObject.SetActive(false);
            ts = card[pos].transform.Find("Word");
            ts.gameObject.SetActive(false);
            ts = card[pos].transform.Find("Bonus");
            ts.gameObject.SetActive(false);
        }
    }

    IEnumerator delayOpen()
    {
        yield return new WaitForSeconds(10f);
        if(openButton.active)
            onOpenButton();
    }

    public void hideFirstCards()
    {
        for (int i = 1; i < playerNum; i++)
            card[i].SetActive(false);
        bid.SetActive(false);
        firstCard.SetActive(false);
    }

    public void setBanker(SFSObject obj)
    {
        bid.SetActive(false);

        string username = obj.GetUtfString("username");
        float value = obj.GetFloat("value");
        int pos = playerPos[username];
        for(int i = 0; i < 5; i ++)
        {
            banker[i].SetActive(i == pos);
            if(i == pos)
            {
                Transform ts = banker[i].transform.Find("Value");
                if (value == 1f)
                    ts.gameObject.GetComponent<Image>().sprite = bankerImage[0];
                else if (value == 1.5f)
                    ts.gameObject.GetComponent<Image>().sprite = bankerImage[1];
                else if (value == 2f)
                    ts.gameObject.GetComponent<Image>().sprite = bankerImage[2];
                ts.gameObject.GetComponent<Image>().SetNativeSize();
            }
        }
    }

    public void dealSecondCards(SFSObject obj)
    {
        firstCard.SetActive(false);
        Transform ts;
        string username = obj.GetUtfString("username");
        int pos = playerPos[username];
        bool isNew = obj.GetBool("isNew");
        bool isArrange = obj.GetBool("isArrange");

        if(pos == 0)
        {
            for (int i = 0; i < 5; i++)
            {
                if(arrangePos[i] == -1)
                    ts = arrange.transform.Find("ShowCards/Card" + (i + 1) + "/Card" + (i + 1));
                else
                    ts = arrange.transform.Find("BackCards/Card" + (arrangePos[i] + 1) + "/Card" + (i + 1));
                ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card" + i)];
            }
            if (!isNew)
            {
                ts = swipeCard.transform.Find("Card1_Land/ShowCard");
                ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card3")];
                ts = swipeCard.transform.Find("Card1_Port/ShowCard");
                ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card3")];
                ts = swipeCard.transform.Find("Card2_Land/ShowCard");
                ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card4")];
                ts = swipeCard.transform.Find("Card2_Port/ShowCard");
                ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card4")];
                ts = swipeCard.transform.Find("Card1_Land");
                ts.gameObject.SetActive(false);
                ts = swipeCard.transform.Find("Card1_Port");
                ts.gameObject.SetActive(true);
                ts = swipeCard.transform.Find("Card2_Land");
                ts.gameObject.SetActive(true);
                ts = swipeCard.transform.Find("Card2_Port");
                ts.gameObject.SetActive(false);

                isSwitch = false;
                GlobalInfo.swipeCardNum = 0;
                swipeCard.SetActive(true);
                bottomImage.SetActive(true);
                openButton.SetActive(true);

                //StartCoroutine(delayOpen());
            }
            else if(!isArrange)
            {
                //if (openButton.active)
                //    openButton.SetActive(true);
                //else
                    arrange.SetActive(true);
                switchCardButton.SetActive(GlobalInfo.cash >= 50 && !isSwitch);
            }
            else
            {
                ts = card[0].transform.Find("BackCards");
                ts.gameObject.SetActive(true);
            }
        }
        else
        {
            card[pos].SetActive(true);
            ts = card[pos].transform.Find("BackCards");
            ts.gameObject.SetActive(true);
            for (int j = 1; j <= 5; j++)
            {
                ts = card[pos].transform.Find("BackCards/Card" + j);
                ts.gameObject.SetActive(true);
            }
        }
    }

    public void autoArrange(SFSObject obj)
    {
        Transform ts;
        int code = obj.GetInt("arrangeCode");
        int[] data = new int[5];
        for(int i = 4; i >= 0; i --)
        {
            data[i] = code % 5;
            code /= 5;
        }
        string str = "";
        for(int i = 0; i < 5; i ++)
        {
            str += data[i].ToString();
            setCard(i, data[i]);
        }
        print(str);
    }

    public void setCard(int cardNo, int backPos)
    {
        Transform ts, ts1;
        int pos = arrangePos[cardNo];
        //print(pos + "," + backPos);
        ts = arrange.transform.Find("DragCard/Card" + (cardNo + 1));
        if(ts == null)
        {
            if (pos == -1)
            {
                ts = arrange.transform.Find("ShowCards/Card" + (cardNo + 1) + "/Card" + (cardNo + 1));
            }
            else
            {
                ts = arrange.transform.Find("BackCards/Card" + (pos + 1) + "/Card" + (cardNo + 1));
            }
        }
        if (backPos == -1)
            ts1 = arrange.transform.Find("ShowCards/Card" + (cardNo + 1));
        else
            ts1 = arrange.transform.Find("BackCards/Card" + (backPos + 1));
        ts.parent = ts1;
        ts.localPosition = new Vector3(0, 0, 0);
        arrangePos[cardNo] = backPos;
    }

    public void hideArrangeCards()
    {
        Transform ts;
        swipeCard.SetActive(false);
        bottomImage.SetActive(false);
        openButton.SetActive(false);
        arrange.SetActive(false);
        card[0].SetActive(true);
        ts = card[0].transform.Find("BackCards");
        ts.gameObject.SetActive(true);
        ts = card[0].transform.Find("ShowCards");
        ts.gameObject.SetActive(false);
        ts = card[0].transform.Find("Word");
        ts.gameObject.SetActive(false);
        ts = card[0].transform.Find("Bonus");
        ts.gameObject.SetActive(false);
    }

    public void showSpecialWords(SFSObject obj)
    {
        Transform ts;
        specialWordsPanel.SetActive(true);
        for(int i = 0; i < 5; i ++)
        {
            ts = specialWordsPanel.transform.Find("Cards/Card" + (i + 1));
            ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card" + i)];
        }
        ts = specialWordsPanel.transform.Find("Word");
        switch (obj.GetInt("type"))
        {
            case 8:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[14];
                break;
            case 5:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[13];
                break;
            case 4:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[12];
                break;
            case 3:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[11];
                break;
            case 2:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[10];
                break;
        }
        ts.gameObject.GetComponent<Image>().SetNativeSize();
        scaleTimer = 0f;
        StartCoroutine(hideSpecialWords());
    }

    IEnumerator hideSpecialWords()
    {
        yield return new WaitForSeconds(4.5f);
        specialWordsPanel.SetActive(false);
    }

    public void showCards(SFSObject obj)
    {
        Transform ts;
        int pos = playerPos[obj.GetUtfString("username")];
        ts = card[pos].transform.Find("BackCards");
        ts.gameObject.SetActive(false);
        ts = card[pos].transform.Find("ShowCards");
        ts.gameObject.SetActive(true);
        for(int i = 0; i < 5; i ++)
        {
            ts = card[pos].transform.Find("ShowCards/Card" + (i + 1));
            ts.gameObject.GetComponent<Image>().sprite = cardImage[obj.GetInt("card" + i)];
        }
    }

    public void showWords(SFSObject obj)
    {
        Transform ts;
        int pos = playerPos[obj.GetUtfString("username")];
        ts = card[pos].transform.Find("Word");
        ts.gameObject.SetActive(true);
        switch (obj.GetInt("type"))
        {
            case 8:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[14];
                break;
            case 5:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[13];
                break;
            case 4:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[12];
                break;
            case 3:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[11];
                break;
            case 2:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[10];
                break;
            case 1:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[obj.GetInt("value")];
                break;
            case 0:
                ts.gameObject.GetComponent<Image>().sprite = bullWords[0];
                break;
        }
        ts.gameObject.GetComponent<Image>().SetNativeSize();
    }

    public void showPayout(SFSObject obj)
    {
        Transform ts;
        int pos = playerPos[obj.GetUtfString("username")];
        int amount = obj.GetInt("amount");
        bool sign = (amount >= 0);
        ts = card[pos].transform.Find("Bonus");
        ts.gameObject.SetActive(true);
        for (int i = 1; i < 5; i ++)
        {
            ts = card[pos].transform.Find("Bonus/Number" + (i + 1));
            ts.gameObject.SetActive(false);
        }
        ts = card[pos].transform.Find("Bonus/Number1");
        if (sign)
            ts.gameObject.GetComponent<Image>().sprite = numberImage[20];
        else
        {
            amount = -amount;
            ts.gameObject.GetComponent<Image>().sprite = numberImage[21];
        }
        ts.gameObject.GetComponent<Image>().SetNativeSize();

        int n = 0, tmp = amount;
        if (amount == 0)
        {
            ts = card[pos].transform.Find("Bonus/Number2");
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = numberImage[0];
            ts.gameObject.GetComponent<Image>().SetNativeSize();
        }
        while (tmp > 0)
        {
            tmp /= 10;
            n++;
        }
        for(int i = n; i > 0; i --)
        {
            ts = card[pos].transform.Find("Bonus/Number" + (i + 1));
            ts.gameObject.SetActive(true);
            ts.gameObject.GetComponent<Image>().sprite = numberImage[amount % 10 + (sign ? 0 : 10)];
            ts.gameObject.GetComponent<Image>().SetNativeSize();
            amount /= 10;
        }
    }

    public void showResult(SFSObject obj)
    {
        bool isWin = obj.GetBool("isWin");
        if (isWin)
            winPanel.SetActive(true);
        else
            losePanel.SetActive(true);
        scaleTimer = 0f;
        StartCoroutine(hideResult());
    }

    IEnumerator hideResult()
    {
        yield return new WaitForSeconds(4.5f);
        winPanel.SetActive(false);
        losePanel.SetActive(false);
    }

    public void hideCards()
    {
        for (int i = 0; i < playerNum; i++)
            card[i].SetActive(false);
        arrange.SetActive(false);
        bid.SetActive(false);
        swipeCard.SetActive(false);
        bottomImage.SetActive(false);
        openButton.SetActive(false);
        firstCard.SetActive(false);
        GlobalInfo.swipeCardNum = 0;
    }

}
