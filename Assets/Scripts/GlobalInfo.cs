﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalInfo
{
    public static bool isLogin = true;
    public static bool isNewLogin = false;
    public static bool isExit = false;
    public static string username;
    public static string password;
    public static string name;
    public static string worldText = "";
    public static string roomText = "";
    public static bool isNew = true;
    public static ISFSObject userInfo = new SFSObject();
    public static RoomItem curRoomItem = new RoomItem();
    public static int swipeCardNum = 0;
    public static int cash = 0;
    public static int searchType = 0;
}
