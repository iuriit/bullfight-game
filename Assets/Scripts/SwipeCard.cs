﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeCard : MonoBehaviour , IPointerDownHandler{

    public GameManager gameManager;
    public GameObject nextCard;

    private Transform showCard;
    public bool isDrag = false;
    private Vector2 prevMousePos;
    private Vector3 prevPos;

	// Use this for initialization
	void Start () {
        showCard = transform.Find("ShowCard");
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetMouseButtonUp(0))
        {
            if(isDrag)
                showCard.localPosition = prevPos;
            isDrag = false;
        }
        if (isDrag)
        {
            if(showCard.localPosition.y >= 0)
            {
                GlobalInfo.swipeCardNum++;
                if(GlobalInfo.swipeCardNum == 2)
                {
                    print("aaaa");
                    gameManager.onOpenButton();
                }
                showCard.localPosition = prevPos;
                isDrag = false;
                this.gameObject.SetActive(false);
                return;
            }
            showCard.position = new Vector3(showCard.position.x, showCard.position.y + (Input.mousePosition.y - prevMousePos.y), showCard.position.z);
            prevMousePos = Input.mousePosition;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDrag = true;
        prevMousePos = Input.mousePosition;
        prevPos = showCard.localPosition;
    }

    public void OnRotate()
    {
        nextCard.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
