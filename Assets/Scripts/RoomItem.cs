﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomItem : MonoBehaviour {
    public Button joinButton;
    public string id;
    public string tableName;
    public int blind;
    public int buyin;
    public int playerNum;
    public int size;
    public bool isActive = false;
    public Sprite[] backImage;

    public void setText()
    {
        Transform ts;
        ts = transform.Find("No/Text");
        ts.gameObject.GetComponent<Text>().text = id;
        ts = transform.Find("Table/Text");
        ts.gameObject.GetComponent<Text>().text = tableName;
        ts = transform.Find("BlindBet/Text");
        ts.gameObject.GetComponent<Text>().text = blind.ToString();
        ts = transform.Find("MinBuy-In/Text");
        ts.gameObject.GetComponent<Text>().text = buyin.ToString();
        ts = transform.Find("Players/Text");
        ts.gameObject.GetComponent<Text>().text = playerNum.ToString() + "/" + size.ToString();
        this.GetComponent<Image>().sprite = backImage[isActive ? 1 : 0];
    }
}
