﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Logging;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class RoomListManager : MonoBehaviour {

    // UI properties

    public GameObject canvas;
    public GameObject mainPanel;
    public GameObject settingsPanel;
    public GameObject exchangeRatePanel;
    public GameObject gameTipPanel;
    public GameObject messagePanel;

    public GameObject[] searchButtons;
    public Sprite[] searchButtonImageOn;
    public Sprite[] searchButtonImageOff;

    public Transform roomListContent;
    public GameObject roomListItem;
    public InputField nameInput;
    public InputField chipInput;
    public InputField cashInput;
    public Text messageText;

    public GameObject longerDisplayCheck;

    //----------------------------------------------------------
    // Private properties
    //----------------------------------------------------------

    private SFSManager sfsManager;

    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    void Awake()
    {
        Application.runInBackground = true;
        if (!SmartFoxConnection.IsInitialized)
        {
            SceneManager.LoadScene("Login");
            return;
        }

        GameObject obj = GameObject.Find("SFSManager");
        sfsManager = obj.GetComponent<SFSManager>();

        onSearch(GlobalInfo.searchType);
        setUserInfo();
        exchangeRatePanel.SetActive(true);
    }

    // UI method

    public void onExit()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void onSearch(int type)
    {
        GlobalInfo.searchType = type;
        for(int i = 0; i < 3; i ++)
            searchButtons[i].GetComponent<Image>().sprite = (i == GlobalInfo.searchType) ? searchButtonImageOn[i] : searchButtonImageOff[i];

        enableUI(false);
        ISFSObject obj = new SFSObject();
        obj.PutInt("type", GlobalInfo.searchType);
        sfsManager.sfs.Send(new ExtensionRequest("getRoomList", obj, null));
    }

    public void onQuicklyJoin()
    {
        RoomItem[] roomArray = roomListContent.GetComponentsInChildren<RoomItem>();
        RoomItem minItem = new RoomItem();
        int minDiff = 6;
        foreach(RoomItem item in roomArray)
        {
            if (item.playerNum == item.size)
                continue;
            if(minDiff > item.size - item.playerNum)
            {
                minDiff = item.size - item.playerNum;
                minItem = item;
            }
        }
        if (minDiff != 6)
        {
            onRoomItemClick(minItem);
        }
    }

    public void onRoomItemClick(RoomItem item)
    {
        if (GlobalInfo.isExit)
            return;
        if(item.playerNum >= item.size)
        {
            messageText.text = "The room is full.";
            messagePanel.SetActive(true);
            return;
        }
        if(item.buyin > GlobalInfo.userInfo.GetInt("chip"))
        {
            messageText.text = "You don't have enough chips.";
            messagePanel.SetActive(true);
            return;
        }
        GlobalInfo.curRoomItem = item;
        sfsManager.sfs.Send(new JoinRoomRequest(item.id));
    }

    public void onNameChange()
    {
        GlobalInfo.name = nameInput.text;
        PlayerPrefs.SetString("name", GlobalInfo.name);
    }

    public void onExchangeRatePanelConfirm()
    {
        exchangeRatePanel.SetActive(false);
        if (PlayerPrefs.GetInt("noGameTipDisplay") != 1)
        {
            gameTipPanel.SetActive(true);
        }
    }

    public void onGameTipPanelLongerDisplayCheckClick()
    {
        longerDisplayCheck.SetActive(!longerDisplayCheck.active);
        PlayerPrefs.SetInt("noGameTipDisplay", longerDisplayCheck.active ? 1 : 0);
    }

    public void onGameTipPanelClose()
    {
        gameTipPanel.SetActive(false);
    }

    public void onMessagePanelClose()
    {
        messagePanel.SetActive(false);
    }

    public void onSettings()
    {
        settingsPanel.SetActive(true);
    }

    public void onSettingsClose()
    {
        settingsPanel.SetActive(false);
    }

    // Public helpers

    public void enableUI(bool flag)
    {
        canvas.GetComponent<CanvasGroup>().interactable = flag;
    }

    public void populateRoomList(SFSObject obj)
    {
        enableUI(true);

        clearRoomList();

        ISFSArray roomArray = obj.GetSFSArray("array");
        bool start = true;
        foreach (ISFSObject item in roomArray)
        {
            GameObject newListItem = Instantiate(roomListItem) as GameObject;
            RoomItem roomItem = newListItem.GetComponent<RoomItem>();
            roomItem.isActive = start;
            roomItem.id = item.GetUtfString("room_id");
            roomItem.tableName = item.GetUtfString("table_name");
            roomItem.blind = item.GetInt("blind");
            roomItem.buyin = item.GetInt("buyin");
            roomItem.size = item.GetInt("size");
            roomItem.playerNum = item.GetInt("player_num");
            roomItem.setText();

            roomItem.joinButton.onClick.AddListener(() => onRoomItemClick(roomItem));

            newListItem.transform.SetParent(roomListContent, false);
            start = false;
        }
    }

    public void clearRoomList()
    {
        foreach (Transform child in roomListContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void setUserInfo()
    {
        nameInput.text = GlobalInfo.name;
        chipInput.text = GlobalInfo.userInfo.GetInt("chip").ToString();
        cashInput.text = "MYR" + (GlobalInfo.userInfo.GetInt("chip") * 0.01f).ToString();
    }
}
