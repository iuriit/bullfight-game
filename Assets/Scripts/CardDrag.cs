﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public static GameObject itemBeingDragged;
    public GameManager gameManager;
    public int cardNo;
    private Transform startParent;

    public void OnBeginDrag(PointerEventData eventData)
    {
        Transform ts;
        for (int i = 0; i < 5; i++)
        {
            if (gameManager.arrangePos[i] == -1)
                ts = gameManager.arrange.transform.Find("ShowCards/Card" + (i + 1) + "/Card" + (i + 1));
            else
                ts = gameManager.arrange.transform.Find("BackCards/Card" + (gameManager.arrangePos[i] + 1) + "/Card" + (i + 1));
            ts.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
        //GetComponent<CanvasGroup>().blocksRaycasts = false;
        itemBeingDragged = this.gameObject;
        startParent = transform.parent;
        transform.parent = gameManager.arrange.transform.Find("DragCard");
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        itemBeingDragged = null;
        if(transform.parent == gameManager.arrange.transform.Find("DragCard"))
        {
            transform.parent = startParent;
            transform.localPosition = new Vector3(0, 0, 0);
        }
        Transform ts;
        for (int i = 0; i < 5; i++)
        {
            if (gameManager.arrangePos[i] == -1)
                ts = gameManager.arrange.transform.Find("ShowCards/Card" + (i + 1) + "/Card" + (i + 1));
            else
                ts = gameManager.arrange.transform.Find("BackCards/Card" + (gameManager.arrangePos[i] + 1) + "/Card" + (i + 1));
            ts.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        //GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

}
